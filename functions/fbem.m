function [outr, outp] = fbem(blade, polar, tsr, B, varargin)
%  [outr, outp] = FBEM(blade, polar, tsr, B)
%
%  This function computes the solution of the BEM problem for the blade given in
%  input, following the approach of Ning (2014) (doi: 10.1002/we.1636).
%  An implementation of Glauert's tip-loss factor is provided
%
%  Input:
%   * blade: a Nsec × 3 matrix: [radius(:), chord(:), twist(:)]
%     radius and chord are in the same units, twist is in RADIANS
%   * polar: a struct containing two fields:
%     - polar.cl, lift coefficient esitmation as function of (alpha, r)
%     - polar.cd, drag coefficient esitmation as function of (alpha, r)
%     both need to be called as CL(alpha, r) with alpha in radians
%     they can be anything that follows this notation (lambda, griddedInterp...)
%     optionally, polar.cm (as above) for the moment around the quarter-chord,
%     this computes the moment along the blade as well
%   * tsr: the tip-speed ratio at which the analysis is carried out (scalar)
%   * B: the number of blades (scalar)
%   * bUseZilic: flag to use Zilic's correction for high induction (def. false)
%
%  Output:
%   * outr: a struct containing quantities varying along the blade span
%     these are: radius (r), induction factors (a, ap), local aoa (alpha),
%     forces (cfnorm, cftang, cl, cd)
%   * outp: a struct containing performance characteristics
%     these are: thrust (ct), torque (cq), power (cp), root-bending moment (cy)
 
if isempty(varargin)
	bUseZilic = false;
else
	bUseZilic = varargin{1};
end

eps = 1e-6;

% short-hands
r = blade(:, 1);
c = blade(:, 2);
beta = blade(:, 3);

rhub = r(1);
rtip = r(end);

% geometrical functions
solid = griddedInterpolant(r, B*c ./ (2*pi*r));
beta = griddedInterpolant(r, beta);

% preallocation
[a, ap, phi, loss] = deal(zeros(size(r)));
for isec = 2:length(r)-1
	if f(pi/2, r(isec)) > 0
		phistar = fzero(@(x) f(x, r(isec)), [eps, pi/2]);
	elseif fpb(-pi/4, r(isec)) > 0 && fpb(eps, r(isec)) > 0
		phistar = fzero(@(x) f(x, r(isec)), [-pi/4, eps]);
	else
		phistar = fzero(@(x) f(x, r(isec)), [pi/2, pi]);
	end

	phi(isec) = phistar;
	a(isec) = af(phistar, r(isec));
	ap(isec) = apf(phistar, r(isec));
	loss(isec) = F(phistar, r(isec));
end
a = interp1(r(2:end), a(2:end), r, 'linear', 'extrap');
ap = interp1(r(2:end), ap(2:end), r, 'linear', 'extrap');
phi = interp1(r(2:end), phi(2:end), r, 'linear', 'extrap');

% reduced quantities
alpha = phi - beta(r);
lsr = tsr * r/rtip;

% blade section coefficients
cl = polar.cl(alpha, r);
cd = polar.cd(alpha, r);

% forces in the turbine frame of reference
cnorm = cl .* cos(phi) + cd .* sin(phi);
cfnorm = cnorm .* c/rtip .* ((1-a).^2 + lsr.^2 .* (1+ap).^2);

ctang = cl .* sin(phi) - cd .* cos(phi);
cftang = ctang .* c/rtip .* ((1-a).^2 + lsr.^2 .* (1+ap).^2);

% forces in the airfoil frame of reference (x: chordwise, y: chord-normal)
cx = cd .* cos(alpha) - cl .* sin(alpha);
cfx = cx .* c/rtip .* ((1-a).^2 + lsr.^2 .* (1+ap).^2);

cy = cd .* sin(alpha) + cl .* cos(alpha);
cfy = cy .* c/rtip .* ((1-a).^2 + lsr.^2 .* (1+ap).^2);

% moment (if present in the polars submitted)
if isfield(polar, 'cm')
	cm = polar.cm(alpha, r);
	cfm = cm .* (c/rtip).^2 .* ((1-a).^2 + lsr.^2 .* (1+ap).^2);
else
	[cm, cfm] = deal(nan(size(cl)));
end

CT = B*rtip / (pi*rtip^2) * trapz(r, cfnorm);
CY = B*rtip / (pi*rtip^3) * trapz(r, r.*cfnorm);

CQ = B*rtip / (pi*rtip^3) * trapz(r, (r).*cftang);
CP = tsr*CQ;

% package for output
% radial distributions
outr = struct('r', r, 'phi', phi, 'a', a, 'ap', ap, 'alpha', alpha, ...
	'cfnorm', cnorm, 'cftang', ctang, 'cfx', cfx, 'cfy', cfy, ...
	'cl', cl, 'cd', cd, 'cm', cm, 'cfm', cfm, 'loss', loss);
% performance
outp = struct('ct', CT, 'cy', CY, 'cq', CQ, 'cp', CP);

% nested functions
	function y = F(phi, r)
		ftip = B/2 .* (rtip - r) ./ (r .* sin(phi));
		Ftip = 2/pi * acos(exp(-ftip));
		fhub = B/2 .* (r - rhub) ./ (r .* sin(phi));
		Fhub = 2/pi * acos(exp(-fhub));
		y = Ftip * Fhub;
	end

	function y = cnf(phi, r)
		aloc = phi - beta(r);
		y = polar.cl(aloc, r) .* cos(phi) + polar.cd(aloc, r) .* sin(phi);
	end

	function y = ctf(phi, r)
		aloc = phi - beta(r);
		y = polar.cl(aloc, r) .* sin(phi) - polar.cd(aloc, r) .* cos(phi);
	end

	function y = k(phi, r)
		y = solid(r) .* cnf(phi, r) ./ (4 * F(phi, r) .* sin(phi).^2);
		if isinf(y)
			y = 1e100;
		end
	end

	function y = kp(phi, r)
 		y = solid(r) .* ctf(phi, r) ./ (4 * F(phi, r) .* sin(phi) .* cos(phi));
		if isinf(y)
			y = -1e100;
		end
	end

	function y = af_buhl(phi, r)
		% Buhl's correction for highly loaded rotors
		g1 = 2 * F(phi, r) .* k(phi, r) - (10/9 - F(phi, r));
		g2 = 2 * F(phi, r) .* k(phi, r) - F(phi, r) .* (4/3 - F(phi, r));
		g3 = 2 * F(phi, r) .* k(phi, r) - (25/9 - 2 * F(phi, r));
		y = (g1 - sqrt(g2)) ./ (g3);
	end

	function y = af_zilic(phi, r)
		% Zilic's correction for highly-loaded rotors
		a0 = 0.2;
		b1 = -1.06;
		b2 = -2 * b1 * a0 + 4 * (1 - 2 * a0) * F(phi, r);
		b3 = a0^2 * (b1 + 4 * F(phi, r));
		eta = 4 * F(phi, r) * k(phi, r);

		% negative solution of (b1-eta)*a^2 + (b2 + 2*eta)*a + (b3-eta) = 0
		beta1 = b1 - eta;
		beta2 = b2 + 2 * eta;
		beta3 = b3 - eta;
		Delta = beta2^2 - 4 * beta1 * beta3;

		% check if solutions can exist (if not, default to Buhl or Prandtl)
		if Delta > 0
			y = (-beta2 + sqrt(Delta))/(2*beta1);
		elseif k(phi, r) > 2/3
			% Buhl
			y = af_buhl(phi, r);
		else
			% Prandtl
			y = af_prandtl(phi, r);			
		end
	end

	function y = af_prandtl(phi, r)
		y = k(phi, r)./(1 + k(phi, r));
	end

	function y = af(phi, r)
		% switch over whcih function to use to compute a(phi)
		if bUseZilic
			if k(phi, r) < 1/4
				y = af_prandtl(phi, r);
			else
				y = af_zilic(phi, r);
			end
		else
			if k(phi, r) < 2/3
				y = af_prandtl(phi, r);
			else
				y = af_buhl(phi, r);
			end
		end
	end

	function y = apf(phi, r)
		y = kp(phi, r)./(1 - kp(phi, r));
	end

	function y = f(phi, r)
		y = sin(phi)./(1 - af(phi, r)) - cos(phi).*(1 - kp(phi, r))./(tsr * r/rtip);
	end

	function y = fpb(phi, r)
		y = sin(phi)./(1 - k(phi, r)) - cos(phi).*(1 - kp(phi, r))./(tsr * r/rtip);
	end

end
