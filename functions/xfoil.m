function [p, cpwr, dump] = xfoil(af, alpha, re, ma, varargin)
% execute xfoil with given alpha, re, and ma
% use re = 0 or inf for inviscid

% file names
file.foil = "foil.xfoil";
file.polar = "polar.xfoil";
file.input = "input.xfoil";
file.output = "output.xfoil";

% delete previous simulations
delete('*.xfoil');

%% make foil file
writematrix(af, file.foil, 'filetype', 'text');

%% make input file
str = "";
% str = str + "plop" + newline + "g" + newline + newline;
str = str + "load" + newline + "foil.xfoil" + newline + "imported" + newline; % load foil file

% set viscid/inviscid
if ~isinf(re) && re ~= 0
	str = str + "oper" + newline; % enter oper
	str = str + "re" + newline + re + newline; % add Re
	str = str + "mach" + newline + ma + newline; % add Mach
	str = str + "visc" + newline;
end
str = str + newline; % back to main

% add further options
for jj = 1:length(varargin)
	command = string(varargin{jj}); % expected: "oper vpar n 5"
	command = regexprep(command, "[ \\\/\n]+", newline);
	command = command + newline + newline + newline;
	str = str + command;
end

% specify dump file
str = str + "oper" + newline + "pacc" + newline + file.polar + newline + "" + newline;

for jj = 1:length(alpha)

	% go to given alpha
	str = str + "a" + newline + alpha(jj) + newline;

	if nargout > 1
		file.dump(jj) = sprintf("dump_%06.3f.xfoil", alpha(jj));
		file.cpwr(jj) = sprintf("cpwr_%06.3f.xfoil", alpha(jj));
	
		% saving dump and cpwr
		str = str + "dump" + newline + file.dump(jj) + newline;
		str = str + "cpwr" + newline + file.cpwr(jj) + newline;
	end
end

% close
str = str + newline + "quit" + newline;

% save input file
writematrix(str, file.input, 'filetype', 'text');

%% run xfoil
if ispc
	[status, result] = system(sprintf("type %s | xfoil >> %s ", file.input, file.output));
else
	error('Not implemented');
end

if status ~= 0
	delete('dump_*.xfoil');
	delete('cpwr_*.xfoil');
	delete(file.polar);
	disp(result);
	error('XFoil terminated with error, check the output files');
end

%% store outputs
% polar as table
p = readtable(file.polar, 'filetype', 'text');

% cpwr and dump as cells of tables
if nargout > 1
	[cpwr, dump] = deal(cell(length(alpha), 1));
	for jj = 1:length(alpha)
		if ~exist(file.dump(jj), 'file')
			% xfoil has not converged, there's nothing to load
			% we leave cpwr{jj} and dump{jj} empty
			continue
		end
		
		% load Cp distribution
		cpwr{jj} = readtable(file.cpwr(jj), 'filetype', 'text', 'numheaderlines', 3, 'ReadVariableNames', false);
		cpwr{jj}.Properties.VariableNames = ["x", "y", "Cp"];
	
		% load dump
		dump{jj} = readtable(file.dump(jj), 'filetype', 'text', 'numheaderlines', 1, 'ReadVariableNames', false);
		dump{jj}.Properties.VariableNames = ["s", "x", "y", "Ue/Vinf", "Dstar", "Theta", "Cf", "H"];
	end
end

%% cleanup
delete('*.xfoil');
