function pitch1 = fpitch(blade, bem, uinf0, uinf1, tsr0, tsr1)
% pitch1 = FPITCH(blade, bem, uinf0, uinf1, tsr0, tsr1)
% Determines the pitch of a passively pitching blade after a change in inflow
% conditions
%
% Inputs:
%  * blade: table or struct containing the following fields:
%   - r: radial coordinates
%   - chord: local chord as a function of r
%   - xp: position of the section quarter-chord with respect to the pitching
%         axis in local chord units along the chord-wise direction
%   - yp: position of the section quarter-chord with respect to the pitching
%         axis in local chord units along the chord-normal direction
%  * bem: struct containing the fields:
%   - r: radial coordinates
%   - tsr: tip-speed ratios at which the BEMT has been run
%   - pitch: pitch angles at which the BEMT has been run
%   - cfx, cfy, and cfm: see the outputs of fbem() for thorough explanation
%  * uinf0: freestream speed before the change in inflow.
%  * uinf1: freestream speed after the change in inflow.
%  * tsr0: tip-speed ratio before the change in inflow.
%  * tsr1: tip-speed ratio after the change in inflow.
%
% Output:
%  * pitch1: pitch of the blade after the change in inflow.

rtip = max(blade.r);
pitch0 = 0;
rho = 1000; % doesn't matter anyway, as it simplifies

% at initial conditions
cfx0 = interpn(bem.r, bem.tsr, bem.pitch, bem.cfx, blade.r, tsr0, pitch0);
cfy0 = interpn(bem.r, bem.tsr, bem.pitch, bem.cfy, blade.r, tsr0, pitch0);
cfm0 = interpn(bem.r, bem.tsr, bem.pitch, bem.cfm, blade.r, tsr0, pitch0);

fx = 1/2 * rho * uinf0^2 * rtip * cfx0;
fy = 1/2 * rho * uinf0^2 * rtip * cfy0;
mr = 1/2 * rho * uinf0^2 * rtip^2 * cfm0;

M0 = trapz(blade.r, ...
	fx .* (blade.yp .* blade.chord) - fy .* (blade.xp .* blade.chord) + mr);

% at final conditions
cfx1 = @(p1) interpn(bem.r, bem.tsr, bem.pitch, bem.cfx, blade.r, tsr1, p1);
cfy1 = @(p1) interpn(bem.r, bem.tsr, bem.pitch, bem.cfy, blade.r, tsr1, p1);
cfm1 = @(p1) interpn(bem.r, bem.tsr, bem.pitch, bem.cfm, blade.r, tsr1, p1);

fx = @(p1) 1/2 * rho * uinf1^2 * rtip * cfx1(p1);
fy = @(p1) 1/2 * rho * uinf1^2 * rtip * cfy1(p1);
mr = @(p1) 1/2 * rho * uinf1^2 * rtip^2 * cfm1(p1);

M1 = @(p1) trapz(blade.r, ...
	fx(p1) .* (blade.yp .* blade.chord) - fy(p1) .* (blade.xp .* blade.chord) + mr(p1));

% solve
pitch1 = fsolve(@(x) M1(x) - M0, 0, optimoptions('fsolve', 'display', 'off'));
