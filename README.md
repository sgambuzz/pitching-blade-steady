# Pitching blade steady

Reduced order code to predict the time-averaged performance of a turbine equipped with passively pitching blades

## Description
The purpose of this code is that of predicting the pitch angle of a passively pitching blade when the inflow and operating conditions are changed.
The mathematical framework behind this is outlined in the white paper included in the folder `documentation`.
This folder also includes a more detailed guide to use this code.

Note: this program requires XFoil to be installed in your system and its folder included in the system PATH.
