classdef Curve < handle
	properties
		points = [];
		arcLength = [];
	end
	
	methods (Access = private)
		function obj = removeDuplicates(obj)
			obj.points = unique(obj.points, 'rows', 'stable');
		end

		function obj = getArcLength(obj)
			d = diff(obj.points, 1);
			obj.arcLength = [0; cumsum(sqrt(sum(d.^2, 2)))];
		end
	end

	methods (Static)
		function y = isAxes(x)
			try
			    y = strcmp(get(x, 'type'), 'axes');
			catch
				y = false;
			end
		end
	end

	methods
		function obj = Curve(points_mat)
			obj.points = points_mat;
			obj.removeDuplicates;
		end
		
		function obj = reinterpolate(obj, varargin)
			if isempty(varargin)
				N = size(obj.points, 1);
			else
				N = varargin{1};
			end
			if isempty(obj.arcLength) || ~isequal(length(obj.arcLength), size(obj.points, 1))
				obj.getArcLength;
			end
			snew = linspace(obj.arcLength(1), obj.arcLength(end), N);
			obj.points = interp1(obj.arcLength, obj.points, snew);
			obj.getArcLength;
		end

		function plot(obj, varargin)
			defopts = {'dataaspectratio', [1, 1, 1], 'nextplot', 'add'};
			if isempty(varargin)
				ax = axes(defopts{:});
			else
				if obj.isAxes(varargin{1})
					ax = varargin{1};
					varargin = varargin{2:end};
				else
					ax = axes(defopts{:});
				end
			end

			plot(ax, obj.points(:, 1), obj.points(:, 2), varargin{:});
		end
	end
end

