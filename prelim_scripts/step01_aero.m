% This script is run to generate, starting from the turbine geometry, the areo
% characteristics of each section of the blade.
% xfoil provides the estiamtes of viscid cl, cd, and cm for low angles of
% attack, Viterna's method then etrapolates these to 360 degrees.
% For Cm, we approximate Cm ≅ Cn * c/4 in deep-stall conditions.

clear;
close all;

%% input
folder_main = "../";
folder_geom = folder_main + "data/geometry/";
folder_aero = folder_main + "data/aero/";
if ~exist(folder_aero, 'dir')
	mkdir(folder_aero);
end

% operating conditions
uinf = 0.5;
tsr = 6.45;
nu = 1e-6;

%%% xfoil options
alpha = -5:0.5:20;
xfoilopts = {...
	'oper vpar xtr 0.25 0.25', ...
	'oper iter 500', ...
	'ppar n 151', ...
	'norm'};

%% adding to path
% will be removed once the code has run, for xfoil(), progress() and Curve()
addpath(folder_main + "functions");
addpath(folder_main + "classes");

%% computations
% turbine geometry
blade = readtable(folder_geom + "blade_table.csv");

% reynolds along blade
rtip = max(blade.r);
lsr = tsr * blade.r/rtip; % local speed ratio
rec = uinf * sqrt(1 + lsr.^2) .* blade.chord / nu; % assuming no induction

% for each blade section:
%	compute polar
%	extrapolate
%	store
out = struct();
nsec = size(blade, 1);
for isec = 1:nsec
	% load and sanitise
	fname = folder_geom + blade.af{isec};
	af = Curve(readmatrix(fname));
	af.reinterpolate(152);
	
	% open TE
	af.points(end, :) = [];


	% compute polar
	p = xfoil(af.points(1:end-1, :), alpha, rec(isec), 0, xfoilopts{:});

	% interpolate (fill missing linearly)
	p = table2array(p(:, [1, 2, 3, 5]));
	p = interp1(p(:, 1), p, alpha, 'linear', 'extrap');
	p(:, 1) = deg2rad(p(:, 1)); % angles in radians

	% extrapolate
	[p360(:, 1), p360(:, 2), p360(:, 3), cn360] = vitExtrapolation(p(:, 1), p(:, 2), p(:, 3));

	% cm as well
	p360(:, 4) = interp1(p(:, 1), p(:, end), p360(:, 1), 'linear', nan);
	p360(isnan(p360(:, 4)), 4) = -1/4 * cn360(isnan(p360(:, 4)));
	
	% initialise
	if ~isfield(out, 'cl')
		out.alpha = p360(:, 1);
		[out.cl, out.cd, out.cm] = deal(zeros(size(p360, 1), size(blade, 1)));
	end

	% store
	out.cl(:, isec) = p360(:, 2);
	out.cd(:, isec) = p360(:, 3);
	out.cm(:, isec) = p360(:, 4);

	% update progress bar
	progress(isec/nsec);
end
out.r = blade.r; % add to output

%% remove from path
rmpath(folder_main + "functions");
rmpath(folder_main + "classes");

%% save
save(folder_aero + "polars.mat", "-struct", "out");


