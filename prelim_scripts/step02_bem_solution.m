% this script computes the solution of the BEM problem for the given turbine,
% for a given set of tip-speed ratios and pitch angles.

clear;
close all;

%% input
folder_main = "../";
folder_geom = folder_main + "data/geometry/";
folder_aero = folder_main + "data/aero/";
folder_bemt = folder_main + "data/bem/";
if ~exist(folder_bemt, 'dir')
	mkdir(folder_bemt);
end

% solution is computed as a function of tsr and pitch only 
% (assume reynolds independence)
tsr = 3:0.25:10;
pitch = deg2rad(-10:0.25:20);

tsr = tsr(:);
pitch = pitch(:);

fname_in = "polars.mat";
fname_out = "fbem.mat";
bUseZilic = false; % flag to use Zilic's high induction correction

%% adding to path
% will be removed at the end of the script, for fbem() and progress()
addpath(folder_main + "functions");

%% computations
polars = load(folder_aero + fname_in);
blade = readtable(folder_geom + "blade_table.csv");

p.cl = griddedInterpolant({polars.alpha, polars.r},  polars.cl);
p.cd = griddedInterpolant({polars.alpha, polars.r},  polars.cd);
p.cm = griddedInterpolant({polars.alpha, polars.r}, -polars.cm); % minus in front as we need positive ccw (inverse of xfoil)

% format blade table as fbem expects it
blade = [blade.r, blade.chord, deg2rad(blade.theta)];

% compute solution of the BEM
total = length(tsr)*length(pitch);
ctr = @(it, ip) (it-1) * length(pitch) + ip;
[outr, outp] = deal(cell(length(tsr), length(pitch)));
for it = 1:length(tsr)
	for ip = 1:length(pitch)
		blade_pitched = blade;
		blade_pitched(:, 3) = blade_pitched(:, 3) + pitch(ip);
		[outr{it, ip}, outp{it, ip}] = fbem(blade_pitched, p, tsr(it), 3, bUseZilic);
		
		progress(ctr(it, ip)/total);
	end
end

%% remove from path
rmpath(folder_main + "functions");

%% save
fs = fields(outp{1});
for jj = 1:length(fs)
	out.(fs{jj}) = cellfun(@(x) x.(fs{jj}), outp);
end

r = blade(:, 1);
outr = permute(outr, [3, 1, 2]);
fs = fields(outr{1});
for jj = 1:length(fs)
	if strcmpi(fs{jj}, 'r')
		continue;
	end
	out.(fs{jj}) = cell2mat(cellfun(@(x) x.(fs{jj}), outr, 'uniformoutput', 0));
end

out.r = r;
out.tsr = tsr;
out.pitch = pitch;

save(folder_bemt + fname_out, "-struct", "out");
