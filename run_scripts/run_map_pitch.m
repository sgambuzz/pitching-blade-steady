clear;
close all;

%% input
folder_main = "../";
folder_geom = folder_main + "data/geometry/";
folder_bemt = folder_main + "data/bem/";
folder_results = folder_main + "data/results/";

blade = readtable(folder_geom + "blade_table.csv");
bem = load(folder_bemt + "fbem.mat");

rho = 999; % kg/m3, to compute dimensional values of loads
rtip = max(blade.r);

xp = linspace(-5, 1, 1001);
yp = linspace(-3, 3, 1001).';

uinf_ref = 0.5; % m/s
uinf = 0.7;

tsr_ref = 6.45;
tsr = tsr_ref * uinf_ref/uinf; % to keep constant omega

%% add to path
% needed for fpitch()
addpath(folder_main);

%% computations
[yp, xp] = ndgrid(yp, xp);
pitch = zeros(size(yp));
for jj = 1:numel(yp)
	blade_edit = blade;
	blade_edit.xp = xp(jj) * ones(size(blade, 1), 1);
	blade_edit.yp = yp(jj) * ones(size(blade, 1), 1);
	pitch(jj) = fpitch(blade_edit, bem, uinf_ref, uinf, tsr_ref, tsr);
	
	progress(jj/numel(xp));
end

%% make matrices
% useful constants
Q_ref = 1/2 * rho * uinf_ref^2;
Q_post = 1/2 * rho * uinf^2;
A = pi * rtip^2;

% adimensional
ct_ref = interpn(bem.tsr, bem.pitch, bem.ct, tsr_ref, 0);
ct_post = interpn(bem.tsr, bem.pitch, bem.ct, tsr * ones(size(pitch)), pitch);
ct_rigid = interpn(bem.tsr, bem.pitch, bem.ct, tsr, 0);

cq_ref = interpn(bem.tsr, bem.pitch, bem.cq, tsr_ref, 0);
cq_post = interpn(bem.tsr, bem.pitch, bem.cq, tsr * ones(size(pitch)), pitch);
cq_rigid = interpn(bem.tsr, bem.pitch, bem.cq, tsr, 0);

cp_ref = interpn(bem.tsr, bem.pitch, bem.cp, tsr_ref, 0);
cp_post = interpn(bem.tsr, bem.pitch, bem.cp, tsr * ones(size(pitch)), pitch);
cp_rigid = interpn(bem.tsr, bem.pitch, bem.cp, tsr, 0);

% dimensional
thrust_ref = Q_ref * A * ct_ref;
thrust_post = Q_post * A * ct_post;
thrust_rigid = Q_post * A * ct_rigid;

torque_ref = Q_ref * A * cq_ref;
torque_post = Q_post * A * cq_post;
torque_rigid = Q_post * A * cq_rigid;

power_ref = Q_ref * A * cp_ref;
power_post = Q_post * A * cp_post;
power_rigid = Q_post * A * cp_rigid;

%% save
save(folder_results + "pitchaxis-location.mat", ...
	"pitch", "thrust_*", "torque_*", "power_*", "rho", ...
	"tsr", "tsr_ref", "uinf", "uinf_ref", "xp", "yp");


