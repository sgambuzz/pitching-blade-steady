% span uinf from 0.4 to 0.7 m/s and plot thrust and torque for the given turbine
% geometry, keeping velocity constant (tsr as 1/uinf);

clear;
close all;

%% input
folder_main = "../";
folder_geom = folder_main + "data/geometry/";
folder_bemt = folder_main + "data/bem/";
folder_results = folder_main + "data/results/";

blade = readtable(folder_geom + "blade_table.csv");
bem = load(folder_bemt + "fbem.mat");

rho = 999;
rtip = 0.6;

uinf = 0.4:0.01:0.7;
uinf_ref = 0.5;

tsr_ref = 6.45;

%% add to path
% needed for fpitch()
addpath(folder_main);

%% compute
tsr = tsr_ref * uinf_ref ./ uinf;
[~, iref] = min(abs(uinf_ref - uinf));

pitch = zeros(size(tsr));
for jj = 1:length(uinf)
	pitch(jj) = fpitch(blade, bem, uinf_ref, uinf(jj), tsr_ref, tsr(jj));
end

%% remove from path
rmpath(folder_main);

%% make outputs
r = blade.r;
omega = tsr .* uinf / rtip;

% query arrays
[rq, tsrq] = ndgrid(r, tsr);
pitchq = repmat(pitch(:).', [length(r), 1]);

% pitching blade performance
ct = interpn(bem.tsr, bem.pitch, bem.ct, tsr, pitch);
cq = interpn(bem.tsr, bem.pitch, bem.cq, tsr, pitch);
cp = interpn(bem.tsr, bem.pitch, bem.cp, tsr, pitch);
thrust = 1/2 * rho * uinf.^2 * pi * rtip^2 .* ct;
torque = 1/2 * rho * uinf.^2 * pi * rtip^3 .* cq;
power = 1/2 * rho * uinf.^3 * pi * rtip^2 .* cp;

% rigid blade performance
ctr = interpn(bem.tsr, bem.pitch, bem.ct, tsr, 0);
cqr = interpn(bem.tsr, bem.pitch, bem.cq, tsr, 0);
cpr = interpn(bem.tsr, bem.pitch, bem.cp, tsr, 0);
thrust_r = 1/2 * rho * uinf.^2 * pi * rtip^2 .* ctr;
torque_r = 1/2 * rho * uinf.^2 * pi * rtip^3 .* cqr;
power_r = 1/2 * rho * uinf.^3 * pi * rtip^2 .* cpr;

% angles of attack
alpha = interpn(bem.r, bem.tsr, bem.pitch, bem.alpha, rq, tsrq, pitchq);
alpha_r = interpn(bem.r, bem.tsr, bem.pitch, bem.alpha, rq, tsrq, zeros(size(pitchq)));


%% save
save(folder_results + "span-uinf-bemt.mat", ...
	'uinf', 'r', ...
	'omega', 'thrust', 'torque', 'power', ...
	'tsr', 'ct', 'cq', 'cp', 'pitch', 'alpha', ...
	'thrust_r', 'torque_r', 'power_r', ...
	'ctr', 'cqr', 'cpr', 'alpha_r');
