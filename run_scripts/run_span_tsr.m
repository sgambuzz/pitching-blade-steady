% span tip-speed ratio in the range between 3 and 10

clear;
close all;

%% input
folder_main = "../";
folder_geom = folder_main + "data/geometry/";
folder_bemt = folder_main + "data/bem/";
folder_results = folder_main + "data/results/";

blade = readtable(folder_geom + "blade_table.csv");
bem = load(folder_bemt + "fbem.mat");

rho = 999;
rtip = max(blade.r);

uinf = 0.5;
tsr = 3:0.25:10;

tsr_ref = 6.45;

%% add to path
% needed for fpitch()
addpath(folder_main);

%% compute
pitch = zeros(size(tsr));
for jj = 1:length(tsr)
	pitch(jj) = fpitch(blade, bem, uinf, uinf, tsr_ref, tsr(jj));
end

%% remove from path
rmpath(folder_main);

%% make outputs
r = blade.r;
omega = tsr .* uinf / rtip;

% query arrays
[rq, tsrq] = ndgrid(r, tsr);
pitchq = repmat(pitch(:).', [length(r), 1]);

% pitching blade performance
ct = interpn(bem.tsr, bem.pitch, bem.ct, tsr, pitch);
cq = interpn(bem.tsr, bem.pitch, bem.cq, tsr, pitch);
cp = interpn(bem.tsr, bem.pitch, bem.cp, tsr, pitch);
thrust = 1/2 * rho * uinf.^2 * pi * rtip^2 .* ct;
torque = 1/2 * rho * uinf.^2 * pi * rtip^3 .* cq;
power = 1/2 * rho * uinf.^3 * pi * rtip^2 .* cp;

% rigid blade performance
ctr = interpn(bem.tsr, bem.pitch, bem.ct, tsr, deg2rad(0.35));
cqr = interpn(bem.tsr, bem.pitch, bem.cq, tsr, deg2rad(0.35));
cpr = interpn(bem.tsr, bem.pitch, bem.cp, tsr, deg2rad(0.35));
thrust_r = 1/2 * rho * uinf.^2 * pi * rtip^2 .* ctr;
torque_r = 1/2 * rho * uinf.^2 * pi * rtip^3 .* cqr;
power_r = 1/2 * rho * uinf.^3 * pi * rtip^2 .* cpr;

% angles of attack
alpha = interpn(bem.r, bem.tsr, bem.pitch, bem.alpha, rq, tsrq, pitchq);
alpha_r = interpn(bem.r, bem.tsr, bem.pitch, bem.alpha, rq, tsrq, zeros(size(pitchq)));

%% save
save(folder_results + "span-tsr-bemt.mat", ...
	'uinf', 'r', ...
	'omega', 'thrust', 'torque', 'power', ...
	'tsr', 'ct', 'cq', 'cp', 'pitch', 'alpha', ...
	'thrust_r', 'torque_r', 'power_r', ...
	'ctr', 'cqr', 'cpr', 'alpha_r');
